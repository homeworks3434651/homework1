package org.homework1.variant1;

public class Contains {
    public static boolean contains(int[] array, int value, int length) {
        for (int i = 0; i < length; i++) {
            if (array[i] == value) {
                return true; // The value is already in the array
            }
        }
        return false; // The value is not in the array
    }
}
