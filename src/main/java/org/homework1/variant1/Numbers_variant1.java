package org.homework1.variant1;

import java.util.Scanner;

import static org.homework1.variant1.PlayHistoricalGame_variant1.playHistoricalGame;
import static org.homework1.variant1.PlayNumberGame.playNumberGame;

public class Numbers_variant1 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Let the game begin!");
        System.out.print("Enter your name: ");
        String name = scanner.nextLine();

        int userLevel;
        while (true) {
            System.out.print("Change the level (easy[1] or history-game[2]): ");
            String userInput = scanner.nextLine().trim(); // Trim the input
            if (userInput.matches("\\d+")) {
                userLevel = Integer.parseInt(userInput);
                if (userLevel == 1 || userLevel == 2) {
                    break;
                } else {
                    System.out.println("Invalid level selection. Please enter 1 or 2: ");
                }
            } else {
                System.out.println("Please enter a valid integer for the level.");
            }
        }

        if (userLevel == 1) {
            playNumberGame(scanner, name);
        } else if (userLevel == 2) {
            playHistoricalGame(scanner, name);
        }

        scanner.close();
    }
}
