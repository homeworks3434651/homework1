package org.homework1.variant1;

import java.util.Random;
import java.util.Scanner;

import static org.homework1.variant1.BubbleSortDescending.bubbleSortDescending;
import static org.homework1.variant1.Contains.contains;


public class PlayHistoricalGame_variant1 {
    public static void playHistoricalGame(Scanner scanner, String name) {
        Random random = new Random();

        System.out.println("Let the historical game begin!");

        String[][] historicalEvents = {
                {"1939", "World War II beginning"},
                {"1969", "Apollo 11 moon landing"},
                {"1776", "Declaration of Independence in the United States"},
                {"1789", "French Revolution beginning"},
                {"1492", "The reaching the Americas of Christopher Columbus"},
                {"1945", "Ending of World War II"},
                {"1963", "Martin Luther King Jr.'s 'I Have a Dream' speech"},
                {"1588", "Spanish Armada defeating by the English"},
                {"1348", "The beginning of the Black Death pandemic in Europe"},
                {"1775", "American Revolutionary War beginning"},
                {"1989", "Falling of the Berlin Wall"},
                {"1215", "Magna Carta signing"},
                {"1865", "Ending of the American Civil War"},
                {"1498", "The reaching India by sea by Vasco da Gama"},
                {"1066", "Battle of Hastings"},
        };

        int randomIndex = random.nextInt(historicalEvents.length);
        String[] selectedEvent = historicalEvents[randomIndex];

        System.out.println("Guess the year of the historical event:");
        System.out.println("When was " + selectedEvent[1]);

        int secretYear = Integer.parseInt(selectedEvent[0]);

        int[] guesses = new int[15];
        int attempt = 0;

        while (true) {
            System.out.print("Enter the year: ");

            while (true) {
                String input = scanner.nextLine().trim(); // Trim leading and trailing spaces

                if (input.isEmpty()) {
                    System.out.print("Enter the year: ");
                    continue;
                }

                try {
                    int userGuess = Integer.parseInt(input);

                    if (contains(guesses, userGuess, attempt)) {
                        System.out.println("You have already entered this year! Please enter a different year: ");
                    } else {
                        guesses[attempt++] = userGuess;

                        if (userGuess < secretYear) {
                            System.out.println("Your guess is \u001B[44;30mtoo early\u001B[0m. Please, try again.");
                            System.out.println("-".repeat(50));
                        } else if (userGuess > secretYear) {
                            System.out.println("Your guess is \u001B[43;31mtoo late\u001B[0m. Please, try again.");
                            System.out.println("-".repeat(50));
                        } else {
                            System.out.println("\u001B[31mCongratulations, " + name + "!\u001B[0m");
                            System.out.println("\u001B[31mYou're right!\u001B[0m");
                            System.out.println("\u001B[0;41m" + selectedEvent[1] + "\u001B[0;31m happened in \u001B[0;41m" + secretYear + "\u001B[0m");
                            System.out.println("\u001B[31mAttempts: " + attempt + "\u001B[0m");
                            // Sort the guesses array in descending order
                            bubbleSortDescending(guesses, attempt);

                            System.out.println("\u001B[31mYour guesses in descending order:\u001B[0m");

                            for (int i = 0; i < attempt; i++) {
                                System.out.print(guesses[i] + " ");
                            }
                            System.out.println("\nGame over!");
                            return; // exit the method
                        }
                        break; // exit the inner loop
                    }
                } catch (NumberFormatException e) {
                    System.out.print("Enter the year: ");
                }
            }

            if (attempt >= guesses.length) {
                System.out.println("Too many attempts. Exiting the game.");
                return;
            }
        }
    }
}
