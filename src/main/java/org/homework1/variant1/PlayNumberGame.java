package org.homework1.variant1;

import java.util.Random;
import java.util.Scanner;

import static org.homework1.variant1.BubbleSortDescending.bubbleSortDescending;
import static org.homework1.variant1.Contains.contains;


public class PlayNumberGame {
    public static void playNumberGame(Scanner scanner, String name) {
        Random random = new Random();

        System.out.println("Let the number game begin!");

        int secretNumber = random.nextInt(101);

        int[] guesses = new int[15];
        int attempt = 0;

        while (true) {
            System.out.print("Enter the number from 0 till 100: ");
            int userGuess;

            while (true) {
                String input = scanner.nextLine().trim(); // Trim leading and trailing spaces

                if (input.isEmpty()) {
                    System.out.print("Enter the number from 0 till 100: ");
                    continue;
                }

                try {
                    userGuess = Integer.parseInt(input);

                    if (contains(guesses, userGuess, attempt)) {
                        System.out.println("You have already entered this number! Please enter a different number from 0 till 100: ");
                    } else if (userGuess >= 0 && userGuess <= 100) {
                        guesses[attempt++] = userGuess;
                        break; // valid input, exit the inner loop
                    } else {
                        System.out.print("Enter the number from 0 till 100: ");
                    }
                } catch (NumberFormatException e) {
                    System.out.print("Enter the number from 0 till 100: ");
                }
            }

            if (userGuess < secretNumber) {
                System.out.println("Your number is \u001B[44;30mtoo small\u001B[0m. Please, try again.");
                System.out.println("-".repeat(50));
            } else if (userGuess > secretNumber) {
                System.out.println("Your number is \u001B[43;31mtoo big\u001B[0m. Please, try again.");
                System.out.println("-".repeat(50));
            } else {
                System.out.println("\u001B[31mCongratulations, " + name + "!\u001B[0m");
                System.out.println("\u001B[31mThe right number is \u001B[0;41m" + userGuess + "\u001B[0m");
                System.out.println("\u001B[31mAttempts: " + attempt + "\u001B[0m");
                // Sort the guesses array in descending order
                bubbleSortDescending(guesses, attempt);

                System.out.println("\u001B[31mYour numbers in descending order:\u001B[0m");
                for (int i = 0; i < attempt; i++) {
                    System.out.print(guesses[i] + " ");
                }

                break;
            }

            if (attempt == guesses.length) {
                System.out.println("Too many attempts. Exiting the game.");
                return;
            }
        }

        System.out.println("\nGame over!");
    }
}
